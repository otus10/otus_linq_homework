﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.Helpers
{
   public static class StringSeparators
   {
      public static string SeparationEndLine => Environment.NewLine;

      public static string RowSeparationLine => $"{SeparationEndLine}======================================={SeparationEndLine}";

      public static string InnerRowSeparationLine => $"{SeparationEndLine}---------------------------------------{SeparationEndLine}";

   }
}
