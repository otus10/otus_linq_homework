﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.Helpers
{
   public static class Warnings
   {
      public static string UserEmpty => $"User is not found";

      public static string UserHasNoAccounts => $"User has no Accounts";

      public static string UserHasNoOperations => $"User has no Operations";

   }
}
