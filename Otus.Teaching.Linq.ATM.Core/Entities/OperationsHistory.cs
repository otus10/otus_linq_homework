﻿using System;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
   public class OperationsHistory
   {
      public int Id { get; set; }
      public DateTime OperationDate { get; set; }
      public OperationType OperationType { get; set; }
      public decimal CashSum { get; set; }
      public int AccountId { get; set; }
      public decimal CashSumSigned { get => this.OperationType == OperationType.InputCash ? this.CashSum : -1 * this.CashSum; }
      public override string ToString()
      {
         return $"OperationId: {Id}" + Environment.NewLine
              + $"OperationDate: {OperationDate}" + Environment.NewLine
              + $"OperationType: {OperationType}" + Environment.NewLine
              + $"CashSum: {CashSum}";
      }
   }
}