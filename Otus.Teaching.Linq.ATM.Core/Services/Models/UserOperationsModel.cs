﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.Services.Models
{
   public class UserOperationsModel
   {
      public User User { get; }
      public List<OperationsHistory> Operations { get; }
      public UserOperationsModel(User user, List<OperationsHistory> operations)
      {
         User = user;
         Operations = operations;
      }
      public override string ToString()
      {
         StringBuilder result = new StringBuilder();
         if (Operations.Count == 0)
         {
            result.Append(Warnings.UserHasNoOperations);
         }
         else
         {
            result.Append(User);
            result.Append(StringSeparators.RowSeparationLine);
            foreach (OperationsHistory operation in Operations)
            {
               result.Append(operation);
               result.Append(StringSeparators.InnerRowSeparationLine);
            }
         }
         result.Append(StringSeparators.SeparationEndLine);
         return result.ToString();
      }
   }
}
