﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.Services.Models
{
   public class UserAccountsModel
   {
      public User User { get; }
      public List<Account> Accounts { get; }
      public UserAccountsModel(User user, List<Account> accounts)
      {
         User = user;
         Accounts = accounts;
      }
      public override string ToString()
      {
         StringBuilder result = new StringBuilder();
         if (Accounts.Count == 0)
         {
            result.Append(Warnings.UserHasNoAccounts);
         }
         else
         {
            foreach (Account account in Accounts)
            {
               result.Append(account);
               result.Append(StringSeparators.RowSeparationLine);
            }
         }
         return result.ToString();
      }
   }
}
