﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.Services.Models
{
   public class UserCashModel
   {
      public User User { get; }

      public decimal Cash { get; }
      public UserCashModel(User user, decimal cash)
      {
         User = user;
         Cash = cash;
      }
      public override string ToString()
      {
         return $"{User}{Environment.NewLine}Cash:{Cash}";
      }
   }
}
