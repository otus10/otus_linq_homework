﻿using Otus.Teaching.Linq.ATM.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.Services.Models
{
   public class UsersWithCashAboveModel
   {
      public List<UserCashModel> UserCashModels { get; }

      public UsersWithCashAboveModel(List<UserCashModel> userCashModels)
      {
         UserCashModels = userCashModels;
      }
      public override string ToString()
      {
         StringBuilder result = new StringBuilder();
         foreach (UserCashModel userCashModel in UserCashModels)
         {
            result.Append(userCashModel);
            result.Append(StringSeparators.RowSeparationLine);
         }
         result.Append(StringSeparators.SeparationEndLine);
         return result.ToString();
      }
   }
}
