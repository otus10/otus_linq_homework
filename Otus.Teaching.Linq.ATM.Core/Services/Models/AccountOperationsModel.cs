﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.Linq.ATM.Core.Helpers;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.Core.Services.Models
{
   public class AccountOperationsModel
   {
      public List<Account> Accounts { get; }
      public List<OperationsHistory> AccountsOperations { get; }
      public AccountOperationsModel(List<Account> accounts, List<OperationsHistory> accountsOperations)
      {
         Accounts = accounts;
         AccountsOperations = accountsOperations;
      }
      public override string ToString()
      {
         StringBuilder result = new StringBuilder();
         if (Accounts.Count == 0)
         {
            result.Append(Warnings.UserHasNoAccounts);
         }
         else
         {
            foreach (Account account in Accounts)
            {
               result.Append(account);
               result.Append(StringSeparators.RowSeparationLine);
               foreach (OperationsHistory operation in AccountsOperations.Where(w => w.AccountId == account.Id))
               {
                  result.Append(operation);
                  result.Append(StringSeparators.InnerRowSeparationLine);
               }
               result.Append(StringSeparators.RowSeparationLine);
            }
         }
         return result.ToString();
      }
   }
}
