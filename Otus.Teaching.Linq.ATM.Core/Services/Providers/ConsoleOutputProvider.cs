﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.Services.Providers
{
   public class ConsoleOutputProvider : IOutputProvider
   {
      public void Output(object obj)
      {
         Console.WriteLine(obj.ToString());
         OutputSeparationEndLine();
      }

      public static void OutputSeparationEndLine()
      {
         Console.WriteLine(Environment.NewLine);
      }
      public static void OutputRowSeparationLine()
      {
         Console.WriteLine("=======================================");
      }
      public static void OutputInnerRowSeparationLine()
      {
         Console.WriteLine("---------------------------------------");
      }

   }
}
