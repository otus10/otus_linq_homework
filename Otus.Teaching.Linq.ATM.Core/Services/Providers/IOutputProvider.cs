﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.Services.Providers
{
   public interface IOutputProvider
   {
      public void Output(object obj);
   }
}
