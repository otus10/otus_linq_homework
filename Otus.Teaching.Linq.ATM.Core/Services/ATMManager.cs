﻿using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Linq;
using System;

using Otus.Teaching.Linq.ATM.Core.Services.Models;
using Otus.Teaching.Linq.ATM.Core.Services.Providers;
using Otus.Teaching.Linq.ATM.Core.Helpers;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
   public class ATMManager
   {
      public IEnumerable<Account> Accounts { get; private set; }

      public IEnumerable<User> Users { get; private set; }

      public IEnumerable<OperationsHistory> History { get; private set; }

      private readonly IOutputProvider _outputProvider;

      public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history, IOutputProvider outputProvider)
      {
         Accounts = accounts;
         Users = users;
         History = history;
         _outputProvider = outputProvider;
      }

      public User GetUser(string login, string password)
      {
         login = login ?? throw new ArgumentNullException(nameof(login));
         password = password ?? throw new ArgumentNullException(nameof(password));

         User user = Users.Where(w => w.Login == login && w.Password == password).FirstOrDefault();
         return user;
      }

      private IEnumerable<Account> GetUserAccounts(User user)
      {
         IEnumerable<Account> accounts = Accounts.Where(w => w.UserId == user.Id);
         return accounts;
      }
      private IEnumerable<OperationsHistory> GetUserOperationsHistory(User user)
      {
         IEnumerable<OperationsHistory> accountOperations = from userAccounts in GetUserAccounts(user)
                                                            join operations in History on userAccounts.Id equals operations.AccountId
                                                            select operations;
         return accountOperations;

      }
      private IEnumerable<OperationsHistory> GetInputCashOperationsHistory()
      {
         IEnumerable<OperationsHistory> accountOperations = History.Where(w => w.OperationType == OperationType.InputCash);
         return accountOperations;
      }
      private IEnumerable<OperationsHistory> GetAccountsAbove(int sum)
      {
         IEnumerable<OperationsHistory> accountOperations = History;
         return accountOperations;
      }

      public void OutputUserInfo(string login, string password)
      {
         User user = GetUser(login, password);
         if (user == null)
         {
            _outputProvider.Output(Warnings.UserEmpty);
         }
         else
         {
            _outputProvider.Output(user);
         }
         
      }
      public void OutputUserAccounts(User user)
      {
         user = user ?? throw new ArgumentNullException(nameof(user));
         UserAccountsModel userAccountsModel = new UserAccountsModel(user, GetUserAccounts(user).ToList());
         _outputProvider.Output(userAccountsModel);
      }
      public void OutputUserOperationsHistory(User user)
      {
         user = user ?? throw new ArgumentNullException(nameof(user));

         List<Account> accounts = GetUserAccounts(user).ToList();
         List<OperationsHistory> accountsOperations = GetUserOperationsHistory(user).ToList();
         AccountOperationsModel accountOperationsModel = new AccountOperationsModel(accounts, accountsOperations);
         _outputProvider.Output(accountOperationsModel);
      }
      public void OutputInputCashOperationsHistory()
      {
         List<UserOperationsModel> userOperations = (
                                                     from users in Users
                                                     join accounts in Accounts on users.Id equals accounts.UserId
                                                     join inputOperations in GetInputCashOperationsHistory() on accounts.Id equals inputOperations.AccountId
                                                     group new { users, accounts, inputOperations } by users into g
                                                     select new UserOperationsModel(g.Key, g.Select(s => s.inputOperations).ToList())
                                                    ).ToList();
         foreach (UserOperationsModel userOperation in userOperations)
         {
            _outputProvider.Output(userOperation);
         }
      }

      public void OutputUsersWithCashAbove(decimal sum)
      {
         List<UserCashModel> usersCash = (
                                          from users in Users
                                          join accounts in Accounts on users.Id equals accounts.UserId
                                          join operations in History on accounts.Id equals operations.AccountId
                                          group new { users, accounts, operations } by users into g
                                          where g.Select(s => s.operations.CashSumSigned).Sum() >= sum
                                          select new UserCashModel(g.Key, g.Select(s => s.operations.CashSumSigned).Sum())
                                         ).ToList();
         UsersWithCashAboveModel usersWithCashAboveModel = new UsersWithCashAboveModel(usersCash);
         _outputProvider.Output(usersWithCashAboveModel);
      }
   }
}