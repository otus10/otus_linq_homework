﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.Core.Services.Providers;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
   class Program
   {
      static void Main(string[] args)
      {
         System.Console.WriteLine("Старт приложения-банкомата...");

         var atmManager = CreateATMManager();

         //TODO: Далее выводим результаты разработанных LINQ запросов

         System.Console.WriteLine($"Вывод информации о заданном аккаунте по логину и паролю{Environment.NewLine}");
         atmManager.OutputUserInfo("snow", "111");
         atmManager.OutputUserInfo("snow", "11123");

         System.Console.WriteLine($"Вывод данных о всех счетах заданного пользователя;{Environment.NewLine}");
         User user = atmManager.GetUser("snow", "111");
         atmManager.OutputUserAccounts(user);

         System.Console.WriteLine($"Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту;{Environment.NewLine}");
         atmManager.OutputUserOperationsHistory(user);

         System.Console.WriteLine($"Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта;{Environment.NewLine}");
         atmManager.OutputInputCashOperationsHistory();

         System.Console.WriteLine($"Вывод данных о всех пользователях у которых на счёте сумма больше N(N задаётся из вне и может быть любой);{Environment.NewLine}");
         atmManager.OutputUsersWithCashAbove(100m);

         System.Console.WriteLine("Завершение работы приложения-банкомата...");
      }

      static ATMManager CreateATMManager()
      {
         using var dataContext = new ATMDataContext();
         var users = dataContext.Users.ToList();
         var accounts = dataContext.Accounts.ToList();
         var history = dataContext.History.ToList();
         IOutputProvider outputProvider = new ConsoleOutputProvider();
         return new ATMManager(accounts, users, history, outputProvider);
      }
   }
}